class Application{
	public static void main (String[] args){
		//first cat
		Cat bob = new Cat();
		bob.name = "bob";
		bob.isPettable = false;
		bob.isSleeping = false;
		
		//second cat
		Cat tim = new Cat();
		tim.name = "tim";
		tim.isPettable = true;
		tim.isSleeping = true;
		
		//array of cats
		Cat[] clowder = new Cat[3];
		clowder[0] = bob;
		clowder[1] = tim;
		//3rd cat
		clowder[2] = new Cat();
		clowder[2].name = "jack";
		clowder[2].isPettable = false;
		clowder[2].isSleeping = true;
		System.out.println(clowder[2].isPettable);
		System.out.println(clowder[2].name);
		System.out.println(clowder[2].isSleeping);
		
		
		//System.out.println(bob.name);
		//System.out.println(tim.name);
		
		//tim.willAttack(tim.isPettable);
		//bob.willAttack(bob.isPettable);
		//tim.isCatAsleep(tim.isSleeping);
		//bob.isCatAsleep(bob.isSleeping);
	}
}